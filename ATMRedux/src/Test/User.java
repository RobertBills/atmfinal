package Test;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class User {
	private String firstName;
	private String lastName;
	private String uuid;
	private byte[] pinHash;
	private ArrayList<Account> accounts;
	private int a;

	public User(String firstName, String lastName, String PIN, Bank theBank) {
		// set User's name
		this.firstName = firstName;
		this.lastName = lastName;

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			this.pinHash = md.digest(PIN.getBytes());
		} catch (NoSuchAlgorithmException e) {
			System.err.println("error, caught NoSuchAlgorithmException");
			e.printStackTrace();
			System.exit(1);
		}
		// get a new, unique universal ID for the user
		this.uuid = theBank.getNewUserUUID();

		this.accounts = new ArrayList<Account>();
		// prints Log message
		System.out.printf("New user %s, %s with ID %s created. \n", lastName, firstName, this.uuid);
	}

	/**
	 * 
	 * @param anAcct
	 */
	public void addAccount(Account account) {
		this.accounts.add(account);
	}

	/**
	 * 
	 * @return
	 */

	public String getUUID() {
		return this.uuid;

	}

	public boolean validatePin(String aPin) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			return MessageDigest.isEqual(md.digest(aPin.getBytes()), this.pinHash);
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Error, caught NoSuchAlgorithmException");
			e.printStackTrace();
			System.exit(1);
		}

		return false;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void printPreviousTransaction() {
		System.out.printf("\n\n%s's", this.firstName);
		for (int a = 0; a < this.accounts.size(); a++)
			;
		System.out.printf("%d) %s\n", this.accounts.get(a).getSummaryLine());
	}

	// System.out.println();
	public int numAccounts() {
		return this.accounts.size();
	}

	public void printAcctTransHistory(int acctIdx) {
		this.accounts.get(acctIdx).printTransHistory();
	}

	public double getAcctBalance(int acctIdx) {
		return this.accounts.get(acctIdx).getBalance();
	}

	public String getAcctUUID(int acctIdx) {
		return this.accounts.get(acctIdx).getUUID();
	}

	public void addAcctTransaction(int acctIdx, double amount, String memo) {
		this.accounts.get(acctIdx).addTransaction(amount, memo);
	}

	public void printAccountsSummary() {
		System.out.println("++++++++++++++++++++++++++++++++++++++++++");
		System.out.printf("\n%s's Summmary of Accounts \n", this.firstName);
		System.out.println();
		System.out.println("++++++++++++++++++++++++++++++++++++++++++");
		for (int a = 0; a < this.accounts.size(); a++) {
			System.out.printf("%d) %s\n", a + 1, this.accounts.get(a).getSummaryLine());
		}
		System.out.println();
	}
}