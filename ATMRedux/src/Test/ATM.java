/*
 * Description: The purpose of this project is to simulate an automatic teller machine. 
 * The ATM is used by the customers of a bank. 
 * Each customer has two accounts: a checking account and a savings account. 
 * Each customer also has a customer number and a personal identification number (PIN); both are required to gain access to the accounts. 
 * (In a real ATM, the customer number would be recorded on the magnetic strip of the ATM card. In this simulation, the customer will need to type it in.) 
 * With the ATM, customers can select an account (checking or savings).•The  balance  of  the  selected  account  is  displayed.  
 * Then  the  customer  can deposit  and  withdraw  money.  This  process  is  repeated  until  the  customer chooses to exit. 
 * The details of the user interaction depend on the user interface that you choose for  the  simulation. 
 * You will  develop  two  separate  interfaces:  
 * a  graphical interface  that  closely  mimics  an  actual  ATM  (Figure),  
 * and  a  text-based interface  that  allows  you  to  test 
 * the  ATM  and  bank  classes  without  being distracted by GUI programming.
 * 
 */
package Test;

import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ATM {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);

		Bank theBank = new Bank("The ArBi$ Bank");

		User aUser = theBank.addUser("Robert", "Bills", "9451");

		Account newAccount = new Account("Checking", aUser, theBank);
		aUser.addAccount(newAccount);
		theBank.addAccount(newAccount);
		User curUser;

		while (true) {
			curUser = ATM.mainMenuPrompt(theBank, sc);

			ATM.printUserMenu(curUser, sc);
			

		}
	}
//////////////////////Menu Prompt //////////////////////
	 
	public static User mainMenuPrompt(Bank theBank, Scanner sc) {
		String userID;
		String pin;
		User authUser;
//////////////////////USER/ID PIN //////////////////////
		
		
		
		JFrame Frame = new JFrame("ATM Machine");
		Frame.setSize(350,250);
		Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel panel = new JPanel();
		Frame.add(panel);
		
		panel.setLayout(null);
		
		JLabel label = new JLabel("ID");
		label.setBounds(100, 20, 165, 25);
		panel.add(label);
		
		
		JTextField userText = new JTextField(80);
		userText.setBounds(10, 20, 80, 25);
		panel.add(userText);
		
		JLabel passwordLabel = new JLabel("PIN");
		passwordLabel.setBounds(100, 50, 165, 25);
		panel.add(passwordLabel);
		
		JTextField passwordText = new JTextField(80);
		passwordText.setBounds(100, 50, 165, 25);
		panel.add(passwordText);
		
		JButton button = new JButton("Login");
		button.setBounds(100, 50, 165, 25);
		Frame.setVisible(true);
		
		do {
			
			
			System.out.printf("\n\nWelcome to %s\n\n", theBank.getName());
			System.out.print("Enter User ID: ");
			userID = sc.nextLine();
			System.out.print("Enter PIN (The Code is '9451'): ");
			pin = sc.nextLine();
			

			authUser = theBank.userLogin(userID, pin);

			if (authUser == null) {
				System.out.println("Login Failed! Please recheck the ID/Pin, and try again.");
			}
		} while (authUser == null);
		return authUser;

	}
////////////////////// Print Menu + Account Summary ////////////////////// 
	public static void printUserMenu(User theUser, Scanner sc) {
		theUser.printAccountsSummary();
		int choice;
		do {
			System.out.println();
			System.out.println();
			System.out.printf("Hello %s! We hope your day is well! What would you like to do?", theUser.getFirstName());
			System.out.println();
			System.out.println("\n++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("++++++++++++++++++++++++++++++++++++++++++");
			System.out.println();
			System.out.println("		Select an Option");
			System.out.println();
			System.out.println("    1: Check Your Balance");
			System.out.println("    2: Deposit");
			System.out.println("    3: Withdraw");
			System.out.println("    4: Transfer");
			System.out.println("    5: Exit");
			System.out.println();
			System.out.println("\n++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("++++++++++++++++++++++++++++++++++++++++++");
			System.out.println();
			System.out.println();

			choice = sc.nextInt();

////////////////////// Balance, Deposit, Withdraw, Transfer, Exit //////////////////////
		while (choice < 1 && choice > 5);
		switch (choice) {
		case 1:
			ATM.showTransHistory(theUser, sc);
			System.out.println("++++++");
			System.out.println();
			System.out.println("++++++");
			System.out.println();
			System.out.println("\n");
			break;
		case 2:
			System.out.println("++++++");
			System.out.println();
			ATM.deposit(theUser, sc);
			System.out.println();
			System.out.println("++++++");
			// int amount = Scanner.nextInt();
			// ATM.deposit(amount);
			System.out.println("\n");
			break;
		case 3:
			System.out.println("++++++");
			System.out.println();
			ATM.withdraw(theUser, sc);
			System.out.println();
			System.out.println("++++++");
			// int amount = Scanner.nextInt();
			// withdraw(amount);
			System.out.println("\n");
			break;
		case 4:
			ATM.transferFunds(theUser, sc);
			System.out.println("++++++");
			System.out.println();
			//ATM.transferFunds(theUser, sc);
			System.out.println();
			System.out.println("++++++");
			System.out.println("\n");
			break;
		case 5:
			System.out.println("");
			sc.nextLine();
			System.out.println("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println();
			System.out.println("You have just terminated the program! Thank you for using The ArBi$ Bank!"
					+ "\n To restart the program, please run the program again! by selecting "
					+ "Run > Run As > Java Application!");
			System.out.println();
			System.out.print(" Have a nice day!");
			System.out.println();
			System.out.println("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			System.out.println();
			System.out.println();
			System.out.println("\n END");
			sc.nextLine();
			System.exit(0);
			break;
			
		default:
			System.out.println("Sorry. That choice is invalid. Please Try Again");
			break;
		}

		if (choice < 1 && choice > 5) {
			System.out.println("Sorry. That choice is invalid. Please Try Again");
		}
	}
		while (choice != 5);
			;
		{
			System.out.println("Thank you for using the ArBi$ System!");
			sc.nextLine();

		}
		if (choice != 5) {
			ATM.printUserMenu(theUser, sc);
		}

		else {
			ATM.printUserMenu(theUser, sc);
		}
	}

////////////////////// Show Transaction History and Transfer Funds //////////////////////
	public static void showTransHistory(User theUser, Scanner sc) {
		int theAcct;
		do {
			System.out.printf("Enter the number (1-%d) of the Account \n" + "whose transactions do you want to see?",
					theUser.numAccounts());
			theAcct = sc.nextInt() - 1;
			if (theAcct < 0 || theAcct >= theUser.numAccounts()) {
				System.out.println("Invalid Account. Please try again.");
			}
		} while (theAcct < 0 || theAcct >= theUser.numAccounts());
		theUser.printAcctTransHistory(theAcct);
	}
//////////////////////Transfer Funds //////////////////////
	public static void transferFunds(User theUser, Scanner sc) {
		int fromAcct;
		int toAcct;
		double amount;
		double acctBal;
		// From
		do {
			System.out.printf("Enter the number (1-%d) of the account\n" + 
		"to transfer from: ", theUser.numAccounts()
		);
			fromAcct = sc.nextInt() - 1;
			
			if (fromAcct < 0 || fromAcct >= theUser.numAccounts()) {
				System.out.println("The account that you entered is invalid. Please try again. ");
			}
		} while (fromAcct < 0 || fromAcct >= theUser.numAccounts());
		acctBal = theUser.getAcctBalance(fromAcct);

		// To
		do {
			System.out.printf("Enter the number (1-%d) of the account\n" + "to transfer from: ", theUser.numAccounts());
			toAcct = sc.nextInt() - 1;
			if (toAcct < 0 || toAcct >= theUser.numAccounts()) {
				System.out.println("The account that you entered is invalid. Please try again. ");
			}

		} while (toAcct < 0 || toAcct >= theUser.numAccounts());

		// Transfer
		do {
			System.out.printf("Enter the amount (max %.02f): $", acctBal);
			amount = sc.nextDouble();
			if (amount < 0) {
				System.out.println("Invalid entry. You must input an amount that is greater than zero.");
			} else if (amount > acctBal) {
				System.out.printf("Amount must not be greater than\n" + "balance of $%.02f.\n", acctBal);
			}
		} while (amount < 0 || amount > acctBal);

		// Process Transfer
		theUser.addAcctTransaction(fromAcct, -1 * amount,
				String.format("Transfer to account %s", theUser.getAcctUUID(toAcct)));
		theUser.addAcctTransaction(toAcct, amount,
				String.format("Transfer to account %s", theUser.getAcctUUID(fromAcct)));
	}
////////////////////// Withdraw & Deposit ////////////////////// 
	public static void withdraw(User theUser, Scanner sc) {
		int fromAcct;
		double amount = 0;
		double acctBal = 0;
		String memo;
 
		// get the account to transfer from
		do {
			System.out.printf("Enter the number (1-%d) of the account\n" + "to withdraw from:  ",theUser.numAccounts());
			fromAcct = sc.nextInt() - 1;
			if (amount < 0 || fromAcct >= theUser.numAccounts()) {
				System.out.println("The account that you entered is invalid. Please try again. ");
			}
		} while (fromAcct < 0 || fromAcct >= theUser.numAccounts());
		acctBal = theUser.getAcctBalance(fromAcct);
		do {
			System.out.printf("Enter the amount (max %.02f): $", acctBal);
			amount = sc.nextDouble();
			if (amount < 0) {
				System.out.println("Invalid entry. You must input an amount that is greater than zero.");
			} else if (amount > acctBal) {
				System.out.printf("Amount must not be greater than\n" + "balance of $%.02f.\n", acctBal);
			}
		} while (amount < 0 || amount > acctBal);

		sc.nextLine();
		// GETAMEMEMEMEMEMEMEMEMEMMOOO
		System.out.print("Enter a memo:  ");
		memo = sc.next();
		// withdraw
		theUser.addAcctTransaction(fromAcct, -1 * amount, memo);
	}
	public static void deposit(User theUser, Scanner sc) {
		int toAcct;
		double amount = 0;
		double acctBal = 0;
		String memo;

		// get the account to transfer from
		do {
			System.out.printf("Enter the number (1-%d) of the account\n" + 
		"to withdraw from:  ", theUser.numAccounts());
			toAcct = sc.nextInt() - 1;
			if (amount < 0 || toAcct >= theUser.numAccounts()) {
				System.out.println("The account that you entered is invalid. Please try again. ");
			}
		} while (toAcct < 0 || toAcct >= theUser.numAccounts());
		acctBal = theUser.getAcctBalance(toAcct);
		do {
			System.out.printf("Enter the amount (max %.02f): $", acctBal);
			amount = sc.nextDouble();
			if (amount < 0) {
				System.out.println("Invalid entry. You must input an amount that is greater than zero.");
			} else if (amount > acctBal) {
				System.out.printf("Amount must not be greater than\n" + "balance of $%.02f.\n", acctBal);
			}
		} while (amount < 0 || amount > acctBal);

		sc.nextLine();
		// GETAMEMEMEMEMEMEMEMEMEMMOOO
		System.out.print("Enter a memo:  ");
		memo = sc.next();
		// withdraw
		theUser.addAcctTransaction(toAcct, -1 * amount, memo);
	}
}